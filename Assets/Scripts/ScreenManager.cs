using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScreenManager : MonoBehaviour
{
    public Text _textScore;
    public int score;
    
    void Update()
    {
        _textScore.text = "Score: " + score;
    }
}
